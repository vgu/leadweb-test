var app_root = 'src';
var path = require('path');
var CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
    context: __dirname,
    app_root: app_root,
    entry: [
        'webpack-dev-server/client?http://localhost:8080',
        'webpack/hot/only-dev-server',
        'babel-polyfill',
        __dirname + '/' + app_root + '/index.js',
    ],
    output: {
        path: __dirname + '/src/js',
        publicPath: 'js/',
        filename: 'bundle.js',
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['react-hot', 'babel-loader'],
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                loaders: ['style', 'css', 'sass'],
            },
            {
                test: /\.css$/,
                loaders: ['style', 'css'],
            }
        ],
    },
    resolve: {
        modules: ['node_modules', path.resolve(__dirname)],
        mainFiles: ['index'],
        descriptionFiles: ['package.json'],
        alias: {
            components: __dirname + '/' + app_root + '/components',
            containers: __dirname + '/' + app_root + '/containers',
            actions: __dirname + '/' + app_root + '/actions',
            api:  __dirname + '/' + app_root + '/api'
        }
    },
    devServer: {
        contentBase: __dirname
    },
    plugins: [
        new CleanWebpackPlugin(['css/main.css', 'js/bundle.js'], {
            root: __dirname + '/' + app_root,
            verbose: true,
            dry: false
        })
    ]
};
