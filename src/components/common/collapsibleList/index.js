import React from "react";
import "./styles.scss";

const CollapsibleList = ({ list, activeItemQuery, setActiveItem, titleKey, bodyKey }) => (
    <ul className="list-group">
        {
            list.map((listItem, i) => {
                let bodyCSSClass = "list-group-item-text";
                if (listItem[activeItemQuery.key] === activeItemQuery.value) {
                    bodyCSSClass += " visible";
                }
                return (
                    <li key={ i }
                        className="list-group-item"
                        onClick={ () => setActiveItem(listItem[activeItemQuery.key]) }
                    >
                        <strong>{ listItem[titleKey] }</strong>
                        <div className={ bodyCSSClass }>{ listItem[bodyKey] }</div>
                    </li>
                );
            })
        }
    </ul>
);

export default CollapsibleList;
