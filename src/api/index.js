import fetch from 'isomorphic-fetch';

export const callApi = (url, options) => {
    return fetch(url, options)
        .then(res => res.json()
            .then(json => ({ json, res }))
        )
        .then(({ json, res }) =>
            !res.ok
            ? Promise.reject(json)
            : json
        )
        .then(
            res => ({ res }),
            err => ({ err: 'Something went wrong' })
        );
};
