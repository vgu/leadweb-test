import React from "react";
import { history } from "./configureStore";
import { Router, Route, IndexRoute } from "react-router";
import App from "./containers/app";
import Users from "./containers/users";

const router = (
  <Router history={history}>
    <Route path="/" component={App}>
      <IndexRoute component={Users}/>
    </Route>
  </Router>
);

export { router };