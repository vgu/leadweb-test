import * as users from "./users";

export default function* () {
    yield [
        users.watchPopulate()
    ]
}
