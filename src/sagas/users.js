import { takeEvery } from "redux-saga";
import { call, put } from "redux-saga/effects";
// Actions.
import { USERS_POPULATE, loadUsers } from "actions/users";

// APIs.
import { getUsers } from "api/users";

// Watchers.
export function* watchPopulate() {
    yield takeEvery(USERS_POPULATE, populate);
}

// Routines.
export function* populate () {
    const { res, err } = yield call(getUsers);

    if (err) {
        yield call(console.warn, "Error: ", err);
    } else {
        yield put(loadUsers(res));
    }
}