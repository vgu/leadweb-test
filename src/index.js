import "babel-polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { store } from "./configureStore";
import { router } from "./router";
import App from "./containers/app";

// render the main component
ReactDOM.render(
    <Provider store={ store }>
        { router }
    </Provider>,
    document.getElementById('root')
);
