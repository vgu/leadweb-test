import { LOAD_USERS, SET_VIEWING_USER } from "actions/users";

const initialState = {
    list: [],
    // This should be handled in component state.
    // The only reason why it's in global store is you request to not use internal state at all.
    viewingId: ''
}

export default function users(state = initialState, action) {
    switch (action.type) {
        case LOAD_USERS:
            return {
                ...state,
                list: [ ...action.users ]
            }

        case SET_VIEWING_USER:
            return {
                ...state,
                viewingId: action.id
            }

        default: return state;
    }
}