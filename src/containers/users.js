import React from "react";
import { connect } from "react-redux";

import CollapsibleList from 'components/common/collapsibleList';
import { populateUsers, setViewingUser } from "actions/users";

class Users extends React.Component {

    componentDidMount = () => this.props.populateUsers();

    render = () => (
        <CollapsibleList
            list={ this.props.users.list }
            activeItemQuery={{
                key: 'id',
                value: this.props.users.viewingId
            }}
            setActiveItem={ this.props.setViewingUser }
            titleKey='title'
            bodyKey='body'
        />
    );
}

export default connect(
    ({ users }) => ({ users }),
    {
        populateUsers,
        setViewingUser
    }
)(Users);
