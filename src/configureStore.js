import { createStore, applyMiddleware, compose } from "redux";
import { browserHistory } from "react-router";
import { syncHistoryWithStore, routerMiddleware } from "react-router-redux";
import createSagaMiddleware from "redux-saga";
import { reducers } from "./reducers/index";
import sagas from "./sagas/index";


const sagaMiddleware = createSagaMiddleware();
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Create store.
const store = createStore(
    reducers,
    undefined, // Initial state
    composeEnhancers(applyMiddleware(
        sagaMiddleware,
        routerMiddleware(browserHistory)
    ))
);

sagaMiddleware.run(sagas);

const history = syncHistoryWithStore(browserHistory, store);

export { store, history };
