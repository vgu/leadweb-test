export const LOAD_USERS = "LOAD_USERS";
export const loadUsers = users => ({
    type: LOAD_USERS,
    users
});

export const SET_VIEWING_USER = 'SET_VIEWING_USER';
export const setViewingUser = id => ({
    type: SET_VIEWING_USER,
    id
});

// SAGAs action creators.
export const USERS_POPULATE = "USERS_POPULATE";
export const populateUsers = () => ({ type: USERS_POPULATE });
